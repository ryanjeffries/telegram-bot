package telegrambot

import "encoding/json"

type MessageEntity struct {
	Type   string `json:"type"`
	Offset int    `json:"offset"`
	Length int    `json:"length"`
	URL    string `json:"url"`  // optional
	User   *User  `json:"user"` // optional
}

type PhotoSize struct {
	FileID   string `json:"file_id"`
	Width    int    `json:"width"`
	Height   int    `json:"height"`
	FileSize int    `json:"file_size"` // optional
}

type Audio struct {
	FileID    string `json:"file_id"`
	Duration  int    `json:"duration"`
	Performer string `json:"performer"` // optional
	Title     string `json:"title"`     // optional
	MimeType  string `json:"mime_type"` // optional
	FileSize  int    `json:"file_size"` // optional
}

type Document struct {
	FileID    string     `json:"file_id"`
	Thumbnail *PhotoSize `json:"thumb"`     // optional
	FileName  string     `json:"file_name"` // optional
	MimeType  string     `json:"mime_type"` // optional
	FileSize  int        `json:"file_size"` // optional
}

type Sticker struct {
	FileID    string     `json:"file_id"`
	Width     int        `json:"width"`
	Height    int        `json:"height"`
	Thumbnail *PhotoSize `json:"thumb"`     // optional
	Emoji     string     `json:"emoji"`     // optional
	FileSize  int        `json:"file_size"` // optional
}

type Video struct {
	FileID    string     `json:"file_id"`
	Width     int        `json:"width"`
	Height    int        `json:"height"`
	Duration  int        `json:"duration"`
	Thumbnail *PhotoSize `json:"thumb"`     // optional
	MimeType  string     `json:"mime_type"` // optional
	FileSize  int        `json:"file_size"` // optional
}

type Voice struct {
	FileID   string `json:"file_id"`
	Duration int    `json:"duration"`
	MimeType string `json:"mime_type"` // optional
	FileSize int    `json:"file_size"` // optional
}

type Contact struct {
	PhoneNumber string `json:"phone_number"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"` // optional
	UserID      int    `json:"user_id"`   // optional
}

type Location struct {

}

type Venue struct {
	Location     Location `json:"location"`
	Title        string   `json:"title"`
	Address      string   `json:"address"`
	FoursquareID string   `json:"foursquare_id"` // optional
}

type UserProfilePhotos struct {
	TotalCount int           `json:"total_count"`
	Photos     [][]PhotoSize `json:"photos"`
}

type File struct {
	FileID   string `json:"file_id"`
	FileSize int    `json:"file_size"` // optional
	FilePath string `json:"file_path"` // optional
}

type ReplyKeyboardMarkup struct {
	Keyboard        [][]KeyboardButton `json:"keyboard"`
	ResizeKeyboard  bool               `json:"resize_keyboard"`   // optional
	OneTimeKeyboard bool               `json:"one_time_keyboard"` // optional
	Selective       bool               `json:"selective"`         // optional
}

type KeyboardButton struct {
	Text            string `json:"text"`
	RequestContact  bool   `json:"request_contact"`
	RequestLocation bool   `json:"request_location"`
}

type ReplyKeyboardHide struct {
	HideKeyboard bool `json:"hide_keyboard"`
	Selective    bool `json:"selective"` // optional
}

type InlineKeyboardMarkup struct {
	InlineKeyboard [][]InlineKeyboardButton `json:"inline_keyboard"`
}

type InlineKeyboardButton struct {
	Text              string  `json:"text"`
	URL               *string `json:"url,omitempty"`                 // optional
	CallbackData      *string `json:"callback_data,omitempty"`       // optional
	SwitchInlineQuery *string `json:"switch_inline_query,omitempty"` // optional
}

type CallbackQuery struct {
	ID              string   `json:"id"`
	From            *User    `json:"from"`
	Message         *Message `json:"message"`           // optional
	InlineMessageID string   `json:"inline_message_id"` // optional
	Data            string   `json:"data"`              // optional
}

type ForceReply struct {
	ForceReply bool `json:"force_reply"`
	Selective  bool `json:"selective"` // optional
}

type ChatMember struct {
	User   *User  `json:"user"`
	Status string `json:"status"`
}

type Message struct {
	MessageID             int              `json:"message_id"`
	From                  *User            `json:"from"`                    // optional
	Date                  int              `json:"date"`
	Chat                  *Chat            `json:"chat"`
	ForwardFrom           *User            `json:"forward_from"`            // optional
	ForwardFromChat       *Chat            `json:"forward_from_chat"`       // optional
	ForwardDate           int              `json:"forward_date"`            // optional
	ReplyToMessage        *Message         `json:"reply_to_message"`        // optional
	EditDate              int              `json:"edit_date"`               // optional
	Text                  string           `json:"text"`                    // optional
	Entities              *[]MessageEntity `json:"entities"`                // optional
	Audio                 *Audio           `json:"audio"`                   // optional
	Document              *Document        `json:"document"`                // optional
	Photo                 *[]PhotoSize     `json:"photo"`                   // optional
	Sticker               *Sticker         `json:"sticker"`                 // optional
	Video                 *Video           `json:"video"`                   // optional
	Voice                 *Voice           `json:"voice"`                   // optional
	Caption               string           `json:"caption"`                 // optional
	Contact               *Contact         `json:"contact"`                 // optional
	Location              *Location        `json:"location"`                // optional
	Venue                 *Venue           `json:"venue"`                   // optional
	NewChatMember         *User            `json:"new_chat_member"`         // optional
	LeftChatMember        *User            `json:"left_chat_member"`        // optional
	NewChatTitle          string           `json:"new_chat_title"`          // optional
	NewChatPhoto          *[]PhotoSize     `json:"new_chat_photo"`          // optional
	DeleteChatPhoto       bool             `json:"delete_chat_photo"`       // optional
	GroupChatCreated      bool             `json:"group_chat_created"`      // optional
	SuperGroupChatCreated bool             `json:"supergroup_chat_created"` // optional
	ChannelChatCreated    bool             `json:"channel_chat_created"`    // optional
	MigrateToChatID       int              `json:"migrate_to_chat_id"`      // optional
	MigrateFromChatID     int              `json:"migrate_from_chat_id"`    // optional
	PinnedMessage         *Message         `json:"pinned_message"`          // optional
}

type User struct {
	ID        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"` // optional
	UserName  string `json:"username"`  // optional
}

type APIResponse struct {
	Ok          bool            `json:"ok"`
	Result      json.RawMessage `json:"result"`
	ErrorCode   int             `json:"error_code"`
	Description string          `json:"description"`
}

type Chat struct {
	ID        int    `json:"id"`
	Type      string `json:"type"`
	Title     string `json:"title"`      // optional
	UserName  string `json:"username"`   // optional
	FirstName string `json:"first_name"` // optional
	LastName  string `json:"last_name"`  // optional
}

type Update struct {
	UpdateID           int                 `json:"update_id"`
	Message            *Message            `json:"message,omitempty"`
	EditedMessage      *Message            `json:"edited_message,omitempty"`
	InlineQuery        *InlineQuery        `json:"inline_query,omitempty"`
	ChosenInlineResult *ChosenInlineResult `json:"chosen_inline_result,omitempty"`
	CallbackQuery      *CallbackQuery      `json:"callback_query,omitempty"`
}
