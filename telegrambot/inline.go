package telegrambot

func AnswerInlineQueryCall(token string, inlineQuery interface{}) Result {
	return Post(MethodUrl(token, "answerInlineQuery"), inlineQuery)
}

type InlineQuery struct {
	Id       string `json:"id"`
	Fromt    User `json:"from"`
	Location *Location `json:"location,omitempty"` // Optional
	Query    string `json:"query"`
	Offset   string `json:"offset"`
}

type AnswerInlineQuery struct {
	InlineQueryId     string `json:"inline_query_id"`
	Results           []interface{} `json:"results"`
	CacheTime         int `json:"cache_time,omitempty"`             // Optional
	IsPersonal        bool `json:"is_personal,omitempty"`           // Optional
	NextOffset        string `json:"next_offset,omitempty"`         // Optional
	SwitchPmText      string `json:"switch_pm_text,omitempty"`      // Optional
	SwitchPmParameter string `json:"switch_pm_parameter,omitempty"` // Optional
}

type InlineQueryResultCachedAudio struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	AudioFileId         string `json:"audio_file_id"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultCachedDocument struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	Title               string `json:"title"`
	DocumentFileId      string `json:"document_file_id"`
	Description         string `json:"description,omitempty"`                 // Optional
	Caption             string `json:"caption,omitempty"`                     // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultCachedGif struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	GifFileId           string `json:"gif_file_id"`
	Title               string `json:"title,omitempty"`                       // Optional
	Caption             string `json:"caption,omitempty"`                     // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultCachedMpeg4Gif struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	Mpeg4FileId         string `json:"mpeg4_file_id"`
	Title               string `json:"title,omitempty"`                       // Optional
	Caption             string `json:"caption,omitempty"`                     // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultCachedPhoto struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	PhotoFileId         string `json:"photo_file_id"`
	Title               string `json:"title,omitempty"`                       // Optional
	Description         string `json:"description,omitempty"`                 // Optional
	Caption             string `json:"caption,omitempty"`                     // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultCachedSticker struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	StickerFileId       string `json:"sticker_file_id"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultCachedVideo struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	VideoFileId         string `json:"video_file_id"`
	Title               string `json:"title,omitempty"`                       // Optional
	Description         string `json:"description,omitempty"`                 // Optional
	Caption             string `json:"caption,omitempty"`                     // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultCachedVoice struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	VoiceFileId         string `json:"voice_file_id"`
	Title               string `json:"title,omitempty"`                       // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultArticle struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	Title               string `json:"title,omitempty"`                                 // Optional
	InputMessageContent string `json:"input_message_content,omitempty"`                 // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty,omitempty"` // Optional
	Url                 string `json:"url,omitempty"`                                   // Optional
	HideUrl             bool `json:"hide_url,omitempty"`                                // Optional
	Description         string `json:"description,omitempty,omitempty"`                 // Optional
	ThumbUrl            string `json:"thumb_url,omitempty"`                             // Optional
	ThumbWidth          int `json:"thumb_width,omitempty"`                              // Optional
	ThumbHeight         int `json:"thumb_height,omitempty"`                             // Optional
}

type InlineQueryResultAudio struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	AudioUrl            string `json:"audio_url"`
	Title               string `json:"title,omitempty"`                       // Optional
	Performer           string `json:"performer,omitempty"`                   // Optional
	AudioDuration       int `json:"audio_duration,omitempty"`                 // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultContact struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	PhoneNumber         string `json:"phone_number,omitempty"`                // Optional
	FirstName           string `json:"first_name,omitempty"`                  // Optional
	LastName            string `json:"last_name,omitempty"`                   // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
	ThumbUrl            string `json:"thumb_url,omitempty"`                   // Optional
	ThumbWidth          int `json:"thumb_width,omitempty"`                    // Optional
	ThumbHeight         int `json:"thumb_height,omitempty"`                   // Optional
}

type InlineQueryResultDocument struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	Caption             string `json:"caption,omitempty"`                     // Optional
	DocumnetUrl         string `json:"document_url"`
	MimeType            string `json:"mime_type"`
	Description         string `json:"description,omitempty,omitempty"`       // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
	ThumbUrl            string `json:"thumb_url,omitempty"`                   // Optional
	ThumbWidth          int `json:"thumb_width,omitempty"`                    // Optional
	ThumbHeight         int `json:"thumb_height,omitempty"`                   // Optional
}

type InlineQueryResultGif struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	GifUrl              string `json:"gif_url,omitempty"`                     // Optional
	GifWidth            int `json:"gif_width"`
	GifHeight           int `json:"gif_height"`
	ThumbUrl            string `json:"thumb_url"`
	Title               string `json:"title,omitempty"`                       // Optional
	Caption             string `json:"caption,omitempty"`                     // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultLocation struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	Longitude           float64 `json:"longitude"`
	Latitude            float64 `json:"latitude"`
	Title               string `json:"title"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
	ThumbUrl            string `json:"thumb_url,omitempty"`                   // Optional
	ThumbWidth          int `json:"thumb_width,omitempty"`                    // Optional
	ThumbHeight         int `json:"thumb_height,omitempty"`                   // Optional
}

type InlineQueryResultMpeg4Gif struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	Mpeg4Url            string `json:"mpeg4_url"`
	Mpeg4Width          int `json:"mpeg4_width,omitempty"`                    // Optional
	Mpeg4Height         int `json:"mpeg4_height,omitempty"`                   // Optional
	ThumbUrl            string `json:"thumb_url,omitempty"`                   // Optional
	Title               string `json:"title"`
	Caption             string `json:"caption,omitempty"`                     // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultPhoto struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	PhotoUrl            string `json:"photo_url"`
	ThumbUrl            string `json:"thumb_url"`
	PhotoWidth          int `json:"photo_width,omitempty"`                    // Optional
	PhotoHeight         int `json:"photo_height,omitempty"`                   // Optional
	Title               string `json:"title"`
	Description         string `json:"description,omitempty,omitempty"`       // Optional
	Caption             string `json:"caption,omitempty"`                     // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultVenue struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	Latitude            float64 `json:"latitude"`
	Longitude           float64 `json:"longitude"`
	Title               string `json:"title"`
	Address             string `json:"address"`
	FoursquareId        *InlineKeyboardMarkup `json:"foursquare_id,omitempty"` // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"`  // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"`  // Optional
	ThumbUrl            string `json:"thumb_url,omitempty"`                    // Optional
	ThumbWidth          int `json:"thumb_width,omitempty"`                     // Optional
	ThumbHeight         int `json:"thumb_height,omitempty"`                    // Optional
}

type InlineQueryResultVideo struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	VideoUrl            float64 `json:"video_url"`
	MimeType            float64 `json:"mime_type"`
	ThumbUrl            string `json:"thumb_url"`
	Title               string `json:"title"`
	Caption             string `json:"caption,omitempty"`                     // Optional
	VideoWidth          int `json:"video_width,omitempty"`                    // Optional
	VideoHeight         int `json:"video_height,omitempty"`                   // Optional
	VideoDuration       int `json:"video_duration,omitempty"`                 // Optional
	Description         string `json:"description,omitempty,omitempty"`       // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type InlineQueryResultVoice struct {
	Type                string `json:"type"`
	Id                  string `json:"id"`
	VoiceUrl            float64 `json:"voice_url"`
	Title               string `json:"title"`
	VoiceDuration       int `json:"voice_duration,omitempty"`                 // Optional
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup,omitempty"` // Optional
	InputMessageContent *interface{} `json:"input_message_content,omitempty"` // Optional
}

type ChosenInlineResult struct {
	ResultId        string `json:"result_id"`
	From            User `json:"from"`
	Location        Location `json:"location,omitemepty"`        // Optional
	InlineMessageId string `json:"inline_message_id,omitemepty"` // Optional
	Query           string `json:"query"`
}

type InputTextMessageContent struct {
	MessageText           string `json:"message_text"`
	ParseMode             Location `json:"parse_mode,omitemepty"`           // Optional
	DisableWebPagePreview bool `json:"disable_web_page_preview,omitemepty"` // Optional
}

type InputLocationMessageContent struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type InputVenueMessageContent struct {
	Latitude     float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Title        string `json:"title"`
	Address      string `json:"address"`
	FoursquareId *InlineKeyboardMarkup `json:"foursquare_id,omitempty"` // Optional
}

type InputContactMessageContent struct {
	PhoneNumber string `json:"phone_number"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
}
