package telegrambot

import (
	"net/http"
	"log"
	"fmt"
	"io/ioutil"
	"encoding/json"
)

var token = ""
var ip = ""
var port = ""

type Address struct {
	Port  string
	Ip    string
	Token string
}

func HandleRequests(address *Address) {
	token = address.Token
	ip = address.Ip
	port = address.Port

	http.HandleFunc("/hello", hello)
	http.HandleFunc(("/" + token), webhook)
	log.Fatal(http.ListenAndServe(":" + port, nil))
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello")
	fmt.Println("Endpoint Hit: hello")
}

func webhook(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "https://www." + whatsMyIp() + "/" + token)
	fmt.Println("Endpoint Hit: https://www." + whatsMyIp() + "/" + token)
}

type IpResponse struct {
	Ip string `json:"ip"`
}

func whatsMyIp() string {
	resp, err := http.Get("https://api.ipify.org?format=json")
	defer resp.Body.Close()
	Check(err)
	body, _ := ioutil.ReadAll(resp.Body)

	var response IpResponse
	json.Unmarshal(body, &response)
	return response.Ip
}
