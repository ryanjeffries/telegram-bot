package telegrambot

import (
	"encoding/json"
	"net/http"
	"bytes"
	"io/ioutil"
)

func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func Post(url string, request interface{}) Result {
	requestJsonString, err := json.Marshal(request)
	var requestBytes = []byte(requestJsonString)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBytes))

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	res, err := client.Do(req)
	Check(err)
	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	var result Result
	json.Unmarshal(body, &result)
	return result
}

func Get(url string) Result {
	resp, err := http.Get(url)
	defer resp.Body.Close()
	Check(err)
	body, _ := ioutil.ReadAll(resp.Body)

	var response Result
	json.Unmarshal(body, &response)
	return response
}

type Result struct {
	Ok          bool `json:"ok"`
	Result      json.RawMessage `json:"result"`
	ErrorCode   int `json:"error_code"`
	Description string `json:"descritpion"`
}
