package telegrambot

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
)

func MethodUrl(token, method string) string {
	return fmt.Sprintf("https://api.telegram.org/bot%s/%s", token, method)
}

func SendMethodRequest(token string, MethodRequest interface{}) Result {
	var u = MethodUrl(token, RequestToMethodName(MethodRequest))
	var r = Post(u, MethodRequest)
	return r
}

func GetUpdates(token string, getUpdateMethodRequest GetUpdateMethodRequest) []Update {
	var updates []Update
	json.Unmarshal(Post(MethodUrl(token, "getUpdates"), getUpdateMethodRequest).Result, &updates)
	return updates
}

type SetWebhookMethodRequest struct {
	url string `"json:limit,omitempty"` // Optional
					    //certificate InputFile `"json:limit,omitempty"` // Optional
}

func SetWebhook(setWebhookMethodRequest SetWebhookMethodRequest) {

}

func GetCurrentOffset(token string) int {
	var offset = 0
	var updates = GetUpdates(token, GetUpdateMethodRequest{Offset: offset})
	for _, update := range updates {
		offset = update.UpdateID + 1
	}
	return offset
}

func RequestToMethodName(i interface{}) (methodName string) {
	a := reflect.TypeOf(i)

	var s = strings.Split(a.String(), ".")
	methodName = strings.Replace(s[len(s) - 1], "MethodRequest", "", -1)
	methodName = strings.Replace(methodName, string([]rune(methodName)[0]), strings.ToLower(string([]rune(methodName)[0])), -1)
	return
}

func GetMe(token string) bool {
	return Get(MethodUrl(token, "getMe")).Ok
}

type SendMessageMethodRequest struct {
	ChatId                string `json:"chat_id"`
	Text                  string `json:"text"`
	ParseMode             string `json:"parse_mode,omitempty"`             // Optional
	DisableWebPagePreview bool `json:"disable_web_page_preview,omitempty"` // Optional
	DisableNotification   bool `json:"disable_notification,omitempty"`     // Optional
	ReplyToMessageId      int `json:"reply_to_message_id,omitempty"`       // Optional
	ReplyMarkup           interface{} `json:"reply_markup,omitempty"`      // Optional
}

type ForwardMessageMethodRequest struct {
	ChatId              string `json:"chat_id"`
	FromChatId          string `json:"from_chat_id"`
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	MessageId           int `json:"message_id"`
}

type SendPhotoMethodRequest struct {
	ChatId              string `json:"chat_id"`
	Photo               string `json:"photo"`
	Caption             string `json:"caption,omitempty"`            // Optional
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	ReplyToMessageId    int `json:"reply_to_message_id,omitempty"`   // Optional
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`  // Optional
}

type SendAudioMethodRequest struct {
	ChatId              string `json:"chat_id"`
	Audio               string `json:"audio"`
	Duration            int `json:"duration,omitempty"`              // Optional
	Performer           string `json:"performer,omitempty"`          // Optional
	Title               string `json:"title,omitempty"`              // Optional
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	ReplyToMessageId    int `json:"reply_to_message_id,omitempty"`   // Optional
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`  // Optional
}

type SendDocumentMethodRequest struct {
	ChatId              string `json:"chat_id"`
	Document            string `json:"document"`
	Caption             string `json:"caption,omitempty"`            // Optional
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	ReplyToMessageId    int `json:"reply_to_message_id,omitempty"`   // Optional
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`  // Optional
}

type SendStickerMethodRequest struct {
	ChatId              string `json:"chat_id"`
	Sticker             string `json:"sticker"`
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	ReplyToMessageId    int `json:"reply_to_message_id,omitempty"`   // Optional
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`  // Optional
}

type SendVideoMethodRequest struct {
	ChatId              string `json:"chat_id"`
	Video               string `json:"video"`
	Duration            int `json:"duration,omitempty"`              // Optional
	Width               int `json:"width,omitempty"`                 // Optional
	Height              int `json:"height,omitempty"`                // Optional
	Caption             string `json:"caption,omitempty"`            // Optional
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	ReplyToMessageId    int `json:"reply_to_message_id,omitempty"`   // Optional
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`  // Optional
}

type SendVoiceMethodRequest struct {
	ChatId              string `json:"chat_id"`
	Voice               string `json:"voice"`
	Duration            int `json:"duration,omitempty"`              // Optional
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	ReplyToMessageId    int `json:"reply_to_message_id,omitempty"`   // Optional
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`  // Optional
}

type SendLocationMethodRequest struct {
	ChatId              string `json:"chat_id"`
	Latitude            float64 `json:"latitude"`
	Longitude           float64 `json:"longitude"`
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	ReplyToMessageId    int `json:"reply_to_message_id,omitempty"`   // Optional
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`  // Optional
}

type SendVenueMethodRequest struct {
	ChatId              string `json:"chat_id"`
	Latitude            float64 `json:"latitude"`
	Longitude           float64 `json:"longitude"`
	Title               string `json:"title"`
	Address             string `json:"address"`
	FoursquareId        string `json:"foursquare_id,omitempty"`      //Optional
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	ReplyToMessageId    int `json:"reply_to_message_id,omitempty"`   // Optional
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`  // Optional
}

type SendContactMethodRequest struct {
	ChatId              string `json:"chat_id"`
	PhoneNumber         string `json:"phone_number"`
	FirstName           string `json:"first_name"`
	LastName            string `json:"last_name,omitempty"`          // Optional
	DisableNotification bool `json:"disable_notification,omitempty"` // Optional
	ReplyToMessageId    int `json:"reply_to_message_id,omitempty"`   // Optional
	ReplyMarkup         interface{} `json:"reply_markup,omitempty"`  // Optional
}

type SendChatActionMethodRequest struct {
	ChatId string `json:"chat_id"`
	Action string `json:"action"`
}

type SendUserProfilePhotosMethodRequest struct {
	UserId int `json:"user_id"`
	Offset int `json:"offset,omitempty"` // Optional
	Limit  int `json:"limit,omitempty"`  // Optional
}

type GetFileMethodRequest struct {
	FileId string `json:"file_id"`
}

type KickChatMemberMethodRequest struct {
	ChatId string `json:"chat_id"`
	UserId int `json:"user_id"`
}

type LeaveChatMethodRequest struct {
	ChatId string `json:"chat_id"`
}

type UnbanChatMemberMethodRequest struct {
	ChatId string `json:"chat_id"`
	UserId int `json:"user_id"`
}

type GetChatMethodRequest struct {
	ChatId string `json:"chat_id"`
}

type GetChatAdministratorsMethodRequest struct {
	ChatId string `json:"chat_id"`
}

type GetChatMembersCountMethodRequest struct {
	ChatId string `json:"chat_id"`
}

type GetChatMemberMethodRequest struct {
	ChatId string `json:"chat_id"`
	UserId int `json:"user_id"`
}

type AnswerCallbackQueryMethodRequest struct {
	CallbackQueryId string `json:"callback_query_id"`
	Text            string `json:"text,omitempty"`
	ShowAlert       bool `json:"show_alert,omitempty"`
}

type Webhook struct {
	Url string `json:"url"`
}

type GetUpdateMethodRequest struct {
	Offset  int `json:"offset,omitempty"`  //Optional
	Limit   int `"json:limit,omitempty"`   // Optional
	Timeout int `"json:timeout,omitempty"` // Optional
}
